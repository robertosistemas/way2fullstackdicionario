﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Teste01Way2
{
    class Program
    {

        static void Main()
        {

            int gatosMortos = 0;
            string palavra = string.Empty;

            do
            {
                Console.Clear();

                Console.WriteLine("Escolha uma opção:");
                Console.WriteLine("0 - Sai do programa.");
                Console.WriteLine("1 - Procura a posição de uma palavra no dicionário.");

                System.ConsoleKeyInfo keyMenu = Console.ReadKey(true);

                if (keyMenu.KeyChar.Equals('0'))
                {
                    break;
                }

                if (keyMenu.KeyChar.Equals('1'))
                {
                    Console.Clear();

                    do
                    {
                        Console.WriteLine("Digite uma palavra e pressione enter para pesquisar.");
                        palavra = Console.ReadLine();
                        if (!string.IsNullOrWhiteSpace(palavra))
                        {
                            break;
                        }
                    } while (true);

                    gatosMortos = 0;

                    Dicionario dic = new Dicionario();

                    dic.Interacoes = 10000;
                    dic.IgnoreCaixa = true;
                    dic.IgnoreAcentos = true;

                    int indice = dic.buscaPosicao(palavra, 0, 10000, ref gatosMortos);

                    if (indice < 0)
                    {
                        Console.WriteLine(string.Format("A palavra {0} não foi localizada no dicionário.", palavra));
                    }
                    else
                    {
                        Console.WriteLine(string.Format("A palavra {0} foi localizada na posição {1}.", palavra, indice));
                    }
                    Console.WriteLine(string.Format("Número de gatinhos mortos = {0}", gatosMortos));
                    Console.WriteLine("Pressione uma tecla para continuar...");

                    keyMenu = Console.ReadKey(true);

                }

            } while (true);
        }
    }
}
