﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Teste01Way2
{
    public class ConsultaServicoException : Exception
    {
        public ConsultaServicoException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
