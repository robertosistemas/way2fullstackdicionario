﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Teste01Way2
{
    public class Dicionario
    {

        public Dicionario()
        {
            Interacoes = 10000;
            IgnoreCaixa = true;
            IgnoreAcentos = true;
        }

        /// <summary>
        /// Define o número de interações inicial
        /// </summary>
        public int Interacoes { get; set; }

        /// <summary>
        /// Define se vai ignorar caixa alta ou baixa nas comparações das palavras
        /// </summary>
        public bool IgnoreCaixa { get; set; }

        /// <summary>
        /// Define se vai ignorar os acentos nas comparações das palavras
        /// </summary>
        public bool IgnoreAcentos { get; set; }

        /// <summary>
        /// busca uma palavra no dicionário recursivamente pesquisando uma nova posição até encontrar ou terminar as tentativas
        /// </summary>
        /// <param name="palavraPesquisada"></param>
        /// <param name="indice"></param>
        /// <param name="incremento"></param>
        /// <param name="gatos"></param>
        /// <param name="IgnoreCaixa"></param>
        /// <param name="IgnoreAcentos"></param>
        /// <returns></returns>
        public int buscaPosicao(string palavraPesquisada, int indice, int incremento, ref int gatosMortos)
        {

            string palavraEncontrada = string.Empty;

            if (indice >= 0)
            {
                try
                {
                    // Consulta no webservice pela indice da palavra
                    palavraEncontrada = consultaServico(indice, ref gatosMortos);
                }
                catch
                {
                    palavraEncontrada = "<ERRO>";
                }

                //Se encontrou
                if (comparaTexto(palavraEncontrada, palavraPesquisada, IgnoreCaixa, IgnoreAcentos) == 0)
                {
                    incremento = 0;
                    return indice;
                }

                //Se incremento é <= 0 significa que não encontrou
                if (incremento > 0)
                {
                    if (comparaTexto(palavraEncontrada, "<ERRO>", IgnoreCaixa, IgnoreAcentos) == 0)
                    {
                        indice -= incremento;
                        if (incremento > 3)
                        {
                            incremento = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(incremento / 2)));
                            indice += incremento;
                        }
                        else
                        {
                            incremento -= 1;
                            indice += 1;
                        }
                    }
                    else
                    {
                        if (comparaTexto(palavraEncontrada, palavraPesquisada, IgnoreCaixa, IgnoreAcentos) > 0)
                        {
                            if (incremento > 3)
                            {
                                incremento = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(incremento / 2)));
                                indice -= incremento;
                            }
                            else
                            {
                                incremento -= 1;
                                indice -= 1;
                            }
                        }
                        else if (comparaTexto(palavraEncontrada, palavraPesquisada, IgnoreCaixa, IgnoreAcentos) < 0)
                        {
                            if (incremento % this.Interacoes == 0)
                            {
                                indice += incremento;
                            }
                            else
                            {
                                if (incremento > 3)
                                {
                                    incremento = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(incremento / 2)));
                                    indice += incremento;
                                }
                                else
                                {
                                    incremento -= 1;
                                    indice += 1;
                                }
                            }
                        }
                    }

                    //Não encontrou, continua buscando recursivamente
                    return buscaPosicao(palavraPesquisada, indice, incremento, ref gatosMortos);

                }

            }

            //Não encontrou
            return -1;

        }

        /// <summary>
        /// Compara string para identificar quando são maiores, menores ou iguais
        /// </summary>
        /// <param name="TextoA"></param>
        /// <param name="TextoB"></param>
        /// <param name="IgnoreCaixa"></param>
        /// <param name="IgnoreAcentos"></param>
        /// <returns></returns>
        public int comparaTexto(string TextoA, string TextoB, bool IgnoreCaixa, bool IgnoreAcentos)
        {

            if (IgnoreCaixa && IgnoreAcentos)
            {
                return string.Compare(TextoA.ToUpper(), TextoB.ToUpper(), new CultureInfo("pt-BR"), System.Globalization.CompareOptions.IgnoreNonSpace);
            }
            else if (IgnoreCaixa && !IgnoreAcentos)
            {
                return string.Compare(TextoA.ToUpper(), TextoB.ToUpper(), new CultureInfo("pt-BR"), System.Globalization.CompareOptions.None);
            }
            else if (!IgnoreCaixa && IgnoreAcentos)
            {
                return string.Compare(TextoA, TextoB, new CultureInfo("pt-BR"), System.Globalization.CompareOptions.IgnoreNonSpace);
            }
            else if (!IgnoreCaixa && !IgnoreAcentos)
            {
                return string.Compare(TextoA, TextoB, new CultureInfo("pt-BR"), System.Globalization.CompareOptions.None);
            }

            return 0;

        }

        /// <summary>
        /// Consulta webservice para localizar uma palavra através de seu índice
        /// </summary>
        /// <param name="indice"></param>
        /// <param name="gatosMortos"></param>
        /// <returns></returns>
        public string consultaServico(int indice, ref int gatosMortos)
        {

            gatosMortos += 1;

            try
            {
                string respostaDoServidor = string.Empty;

                WebRequest request = WebRequest.Create(string.Concat("http://teste.way2.com.br/dic/api/words/", indice));
                request.Credentials = CredentialCache.DefaultCredentials;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            respostaDoServidor = reader.ReadToEnd();
                        }
                    }
                }
                return respostaDoServidor;
            }
            catch (Exception e)
            {
                throw new ConsultaServicoException(e.Message, e);
            }
        }
    }
}
