﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Teste01Way2;

namespace Teste01Way2Test
{
    [TestClass]
    public class DicionarioUnitTest
    {
        [TestMethod]
        public void BuscaPosicaoTest()
        {
            int gatosMortos = 0;
            Dicionario dic = new Dicionario();
            dic.Interacoes = 10000;
            dic.IgnoreCaixa = true;
            dic.IgnoreAcentos = true;

            Assert.AreEqual(dic.buscaPosicao("asdasgsd", 0, 10000, ref gatosMortos), -1);

            Assert.AreEqual(dic.buscaPosicao("aarão", 0, 10000, ref gatosMortos), 0);

            Assert.AreEqual(dic.buscaPosicao("escola", 0, 10000, ref gatosMortos), 18340);

        }

        [TestMethod]
        public void comparaTextoTest()
        {
            Dicionario dic = new Dicionario();

            Assert.AreEqual(dic.comparaTexto("casa", "CASA", false, false), -1);

            Assert.AreEqual(dic.comparaTexto("atenção", "AtEncÂo", true, true), 0);

            Assert.AreEqual(dic.comparaTexto("WA2", "florianópolis", true, true), 1);

        }

        [TestMethod]
        public void consultaServicoTest()
        {

            int gatosMortos = 0;

            Dicionario dic = new Dicionario();

            Assert.AreEqual(dic.consultaServico(0, ref gatosMortos), "AARÃO");

        }

    }
}
